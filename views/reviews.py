from flask import request

from app import app


@app.route('/review/book', methods=['GET'])
def get_book_review():
    content = request.json
    user_id = content['user_id']

    id = request.args.get('id')

    return f'user_id: {user_id} id {id}'


@app.route('/review/book', methods=['POST'])
def post_book_review():
    content = request.json
    user_id = content['user_id']

    id = request.args.get('id')

    return f'user_id: {user_id} id: {id}'
