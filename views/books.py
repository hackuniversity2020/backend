from flask import request

from models.book import Book
from app import app
from serializers.book import BookSerializer


@app.route('/books', methods=['GET'])
def get_book_by_id():
    id = request.args.get('id')
    book = Book.find(id)
    response = BookSerializer.JSON(book)

    return response


@app.route('/books/my', methods=['GET'])
def get_books_my():
    content = request.json
    user_id = content['user_id']

    return f'user_id: {user_id}'


@app.route('/books/<genre>', methods=['GET'])
def get_books_by_genre(genre):
    assert genre == request.view_args['genre']
    start = request.args.get('start')
    count = request.args.get('count')

    return f'genre: {genre} start: {start} count: {count}'
