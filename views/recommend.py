from flask import request

from app import app


@app.route('/recommend/my', methods=['GET'])
def get_recommend_my():
    content = request.json
    user_id = content['user_id']

    limit = request.args.get('limit')

    return f'user_id: {user_id} limit: {limit}'


@app.route('/recommend/book', methods=['GET'])
def get_recommend_by_book():
    content = request.json
    user_id = content['user_id']

    id = request.args.get('id')
    limit = request.args.get('limit')

    return f'user_id: {user_id} limit: {limit} id: {id}'

