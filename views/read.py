from flask import request

from app import app


@app.route('/read', methods=['GET'])
def get_recommend_by_book():
    content = request.json
    user_id = content['user_id']

    id = request.args.get('id')

    return f'user_id: {user_id} id: {id}'

