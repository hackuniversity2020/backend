from flask import request

from app import app

from models.user import User


@app.route('/users', methods=['GET'])
def get_users():
    id = request.args.get('id')
    return f'id: {id}'


@app.route('/users/my', methods=['GET'])
def get_users_my():
    content = request.json
    user_id = content['user_id']

    return f'user_id: {user_id}'


@app.route('/users/login', methods=['POST'])
def login():
    content = request.json
    username = content['username']
    password = content['password']

    user = User(username)
    ok = user.verify_password(password)
    if not ok:
        return {
            'error': 'incorrect login or password'
        }
    return user.token


@app.route('/users/signup', methods=['POST'])
def signup():
    content = request.json
    username = content['username']
    password = content['password']

    user = User(username, password)
    ok = user.register()

    if not ok:
        return {
            'error': 'user already exists'
        }
    return {}
