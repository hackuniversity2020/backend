from models.book import Book


class BookSerializer:
    @staticmethod
    def JSON(book: Book) -> dict:
        if book is None:
            return {}

        return {
            "id": book.isbn,
            "title": book.title,
            "author": book.author,
            "publicationyear": book.published_date,
            "image": book.image,
            "price": book.price,
            "genre": book.genre,
        }
