from passlib.hash import bcrypt
from py2neo import Node, NodeMatcher
from py2neo.ogm import GraphObject, Property

from app import graph

matcher = NodeMatcher(graph)


class User(GraphObject):
    __primarykey__ = 'username'

    username = Property()
    password = Property()

    token = Property()

    def __init__(self, username, password=None):
        self.username = username
        if password is not None:
            self.password = bcrypt.encrypt(str(password))

    def find(self, username=None):
        if username is None:
            username = self.username
        graph.begin()
        user = User.match(graph, primary_value=username)
        user = user.first()
        return user

    def register(self):
        if not self.find():
            graph.create(self)
            return True
        else:
            return False

    def verify_password(self, password):
        user = self.find()
        if user is None:
            return False
        if not bcrypt.verify(password, user.password):
            return False
        if user.token is not None:
            self.token = user.token
        else:
            self.password = user.password
            self.token = bcrypt.encrypt(str(self.username))
            graph.push(self)
        return True


#
# u = User('hello')
# # graph.push(u)\
# print(User.find('hello'))
graph.delete_all()
