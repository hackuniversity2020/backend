from passlib.hash import bcrypt
from py2neo import Node, NodeMatcher
from py2neo.ogm import GraphObject, Property

from app import graph


class Book(GraphObject):
    __primarykey__ = 'isbn'

    isbn = Property()
    title = Property()
    publisher = Property()
    published_date = Property()
    genre = Property()
    author = Property()
    price = Property()
    image = Property()

    def __init__(self, data):
        assert 'isbn' in data
        self.isbn = data['isbn']
        for key, value in data.items():
            self.__setattr__(key, value)

    def __repr__(self):
        return f'ISBN: {self.isbn}'

    @staticmethod
    def find(isbn):
        graph.begin()
        book = Book.match(graph, primary_value=isbn).first()
        return book

    @staticmethod
    def list(start, offset):
        graph.begin()
        books = Book.match(graph)
        result = list()
        for i, book in enumerate(books):
            print('book', book)
            if start <= i < start + offset:
                result.append(book)
            if i >= start + offset:
                break
        return result


if __name__ == "__main__":
    # graph.delete_all()
    graph.begin()
    x = Book({'isbn': '1', 'title': 'hello'})
    print(x, x.isbn)
    graph.push(x)

    graph.create(x)
    y = Book({'isbn': 24})
    graph.push(y)

    z = Book({'isbn': 22})
    graph.push(z)

    print(Book.find(23))
    print('list', Book.list(0, 4))
