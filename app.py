from flask import Flask

from py2neo import Graph

# import views
from config import Config

app = Flask(__name__)
config = Config()

graph = Graph(config.NEO4J_URL + '/db/data/', username=config.NEO4J_USERNAME, password=config.NEO4J_PASSWORD)

import views.books
import views.users

if __name__ == '__main__':
    app.run()
