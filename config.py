import os

# from flask_dotenv import DotEnv

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    def __init__(self):
        self.NEO4J_URL = os.environ.get('NEO4J_URL', 'http://localhost:7474')
        self.NEO4J_USERNAME = os.environ.get('NEO4J_USERNAME', 'neo4j')
        self.NEO4J_PASSWORD = os.environ.get('NEO4J_PASSWORD', 'default')

        print(self.NEO4J_URL)
        print(self.NEO4J_USERNAME)
        print(self.NEO4J_PASSWORD)
